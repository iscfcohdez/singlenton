package app;

import deck.Deck;

public class SingletonExcercise {

	public static void main(String[] args) {
		Deck deck = Deck.getUniqueInstance();
	    deck.print( );
	}

}
