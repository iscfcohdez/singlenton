package app;

public enum Suit {
	SPADES,
	HEARTS,
	CLUBS,
	DIAMONDS
}
