package deck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import app.Suit;

public class Deck {

	private List<Card> cards;
	private static Deck uniqueInstance;	
	
	private Deck( ) {
	    cards = new ArrayList<Card>( );

	    // build the deck
	    Suit[] suits = {Suit.SPADES, Suit.HEARTS, Suit.CLUBS, Suit.DIAMONDS};
	    for(Suit suit: suits) {
	      for(int i = 2; i <= 14; i++) {
	        cards.add(new Card(suit, i));
	      }
	    }

	    // shuffle it!
	    Collections.shuffle(cards, new Random( ));
	}

	public void print( ) {
	    for(Card card: cards) {
	      card.print( );
	    }
	}

	public static Deck getUniqueInstance() {
		if(uniqueInstance==null) {
			uniqueInstance = new Deck();
		}
		
		return uniqueInstance;
	}

	public static void setUniqueInstance(Deck uniqueInstance) {
		Deck.uniqueInstance = uniqueInstance;
	}
	  
}
